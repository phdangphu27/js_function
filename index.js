const totalEl = document.getElementById("total");
const score1El = document.getElementById("score1");
const score2El = document.getElementById("score2");
const score3El = document.getElementById("score3");
const areaEl = document.getElementById("area");
const priorityEl = document.getElementById("priority");
const resultEl = document.getElementById("result");

const result = (total, s1, s2, s3, area, priority) => {
  const calc = s1 + s2 + s3 + area + priority;
  if (s1 === 0 || s2 === 0 || s3 === 0) {
    resultEl.innerHTML = "🤦‍♂️ FAILED";
  } else if (calc >= total) {
    resultEl.innerHTML = "👏 PASSED";
  } else {
    resultEl.innerHTML = "🤦‍♂️ FAILED";
  }
};

const check = () => {
  const totalVal = +totalEl.value;
  const score1Val = +score1El.value;
  const score2Val = +score2El.value;
  const score3Val = +score3El.value;

  const areaCheck = (area) => {
    let score = 0;
    if (area === "A") {
      score = 2;
    }
    if (area === "B") {
      score = 1.5;
    }
    if (area === "C") {
      score = 1;
    }
    if (area === "X") {
      score = 0;
    }
    return score;
  };

  const priorityCheck = (subject) => {
    let score = 0;
    if (subject === 1) {
      score = 2.5;
    }
    if (subject === 2) {
      score = 1.5;
    }
    if (subject === 3) {
      score = 1;
    }
    if (subject === 0) {
      score = 0;
    }
    return score;
  };

  const areaVal = areaCheck(areaEl.value);
  const priorityVal = priorityCheck(+priorityEl.value);

  result(totalVal, score1Val, score2Val, score3Val, areaVal, priorityVal);
};

//exercise 2
const userEl = document.getElementById("user");
const amountEl = document.getElementById("amount");
const moneyEl = document.getElementById("money");

const calcHandler = () => {
  const userVal = userEl.value;
  const amountVal = +amountEl.value;

  const calc = (name, amount) => {
    let total = 0;
    if (amount <= 50) {
      total = 500 * amount;
    } else if (amount <= 100) {
      total = 50 * 500 + (amount - 50) * 650;
    } else if (amount <= 200) {
      total = 50 * 500 + 50 * 650 + (amount - 100) * 850;
    } else if (amount <= 350) {
      total = 50 * 500 + 50 * 650 + 100 * 850 + (amount - 150) * 1100;
    } else {
      total =
        50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (amount - 350) * 1300;
    }
    moneyEl.innerHTML = `${name} has to pay ${total} VND`;
  };
  calc(userVal, amountVal);
};
